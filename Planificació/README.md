# Planificació
Es pot veure el seguiment a:

https://trello.com/b/Oer0wutr/treball-final-de-grau-albert-pujadas-g%C3%B3mez

* 2-5 de maig realització de recerca general i documentació.  
* 6-7 de maig  documentació i realització de primers muntatjes.  
* 9-13 de maig realització del treball en si i de les practiques i fer el video.  
* 16-19 finalització del treball, confecció poster i preparació presentació.